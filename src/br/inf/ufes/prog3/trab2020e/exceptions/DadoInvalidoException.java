package br.inf.ufes.prog3.trab2020e.exceptions;

public class DadoInvalidoException extends DomainException {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 6926456989365338355L;

  private String dado;
  
  public DadoInvalidoException(String dado) {
    this.dado = dado;
  }
  
  @Override
  public String getMessage() {
    return "Dado inválido: " + dado + ".";
  }
}
