package br.inf.ufes.prog3.trab2020e.exceptions;

public class CadastroRepetidoException extends DomainException {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = -1061703373155886734L;

  private String referencia;
  
  public CadastroRepetidoException(String referencia) {
    this.referencia = referencia;
  }
  
  @Override
  public String getMessage() {
    return "Cadastro repetido: " + referencia + ".";
  }
}
