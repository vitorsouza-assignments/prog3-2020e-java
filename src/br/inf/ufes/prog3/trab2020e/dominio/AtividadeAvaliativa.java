package br.inf.ufes.prog3.trab2020e.dominio;

import java.util.Date;

/**
 * Representa uma atividade avaliativa e que, portanto, possui um prazo para ser entregue ou realizada.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public abstract class AtividadeAvaliativa extends Atividade {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = -9025800446086238027L;

  /** Data em que a atividade será realizada ou prazo em que tem que ser entregue. */
  private Date prazo;

  /** Construtor. */
  public AtividadeAvaliativa(Disciplina disciplina, String nome, boolean sincrona, Date prazo) {
    super(disciplina, nome, sincrona);
    this.prazo = prazo;
  }
  
  @Override
  public boolean isAvaliativa() {
    return true;
  }
  
  @Override
  public Date getData() {
    return prazo;
  }
}
