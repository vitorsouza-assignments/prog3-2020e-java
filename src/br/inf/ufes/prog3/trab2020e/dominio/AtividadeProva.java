package br.inf.ufes.prog3.trab2020e.dominio;

import java.util.Date;

/**
 * Representa uma atividade de tipo prova: atividade síncrona em que os estudantes respondem a
 * questões elaboradas pelo docente para nota.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class AtividadeProva extends AtividadeAvaliativa {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 6641905692868274573L;

  /** Conteúdo a ser cobrado nas questões da prova. */
  private String conteudo;

  /**
   * @param disciplina
   * @param nome
   * @param sincrona
   * @param prazo
   * @param hora
   * @param conteudo
   */
  public AtividadeProva(Disciplina disciplina, String nome, Date prazo, String conteudo) {
    super(disciplina, nome, true, prazo);
    this.conteudo = conteudo;
  }
}
