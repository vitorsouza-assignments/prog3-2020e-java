package br.inf.ufes.prog3.trab2020e.exceptions;

public abstract class DomainException extends Exception {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = -7257390507910202706L;

}
