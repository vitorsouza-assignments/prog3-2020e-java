package br.inf.ufes.prog3.trab2020e.exceptions;

public class ReferenciaInvalidaException extends DomainException {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 8724951798792909190L;

  private String referencia;
  
  public ReferenciaInvalidaException(String referencia) {
    this.referencia = referencia;
  }
  
  @Override
  public String getMessage() {
    return "Referência inválida: " + referencia + ".";
  }
}
