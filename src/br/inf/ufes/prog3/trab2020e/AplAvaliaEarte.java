package br.inf.ufes.prog3.trab2020e;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;
import java.util.Set;
import br.inf.ufes.prog3.trab2020e.dominio.Disciplina;
import br.inf.ufes.prog3.trab2020e.dominio.Docente;
import br.inf.ufes.prog3.trab2020e.dominio.Estudante;
import br.inf.ufes.prog3.trab2020e.exceptions.DomainException;
import br.inf.ufes.prog3.trab2020e.io.Escritor;
import br.inf.ufes.prog3.trab2020e.io.Leitor;

/**
 * Classe principal que executa o programa.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class AplAvaliaEarte implements Serializable {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 8536766031796285002L;

  /** Nome do arquivo no qual o estado da aplicação é serializado. */
  private static final String ARQUIVO_SERIALIZACAO = "dados.dat";

  /** Nome do arquivo de relatório de visão geral. */
  private static final String RELATORIO_GERAL = "1-visao-geral.csv";

  /** Nome do arquivo de relatório de estatísticas dos docentes. */
  private static final String RELATORIO_DOCENTES = "2-docentes.csv";

  /** Nome do arquivo de relatório de estatísticas dos estudantes. */
  private static final String RELATORIO_ESTUDANTES = "3-estudantes.csv";

  /** Nome do arquivo de relatório de estatísticas das disciplinas de cada docente. */
  private static final String RELATORIO_DISCIPLINAS = "4-disciplinas.csv";

  /** Indica se o programa deve serializar os dados ao invés de gerar as saí­das. */
  private boolean serializar;
  
  /** Conjunto de todos os docentes lidos dos arquivos. */
  private Set<Docente> docentes;
  
  /** Conjunto de todas as disciplinas lidas dos arquivos. */
  private Set<Disciplina> disciplinas;
  
  /** Conjunto de todos os estudantes lidos dos arquivos. */
  private Set<Estudante> estudantes;

  /** Método principal que executa o programa. */
  public static void main(String[] args) {
    AplAvaliaEarte apl = null;
    boolean readOnly = false, writeOnly = false;
    String nomeArquivoPeriodos = null, nomeArquivoDocentes = null, nomeArquivoDisciplinas = null,
        nomeArquivoEstudantes = null, nomeArquivoMatriculas = null, nomeArquivoAtividades = null,
        nomeArquivoAvaliacoes = null;

    // Determina configurações regionais.
    Locale.setDefault(new Locale("pt", "BR"));

    try {
      // Processa os parâmetros da linha de comando.
      for (int i = 0; i < args.length; i++) {
        // Procura pela opção -p, que especifica o arquivo de periodos.
        if ("-p".equals(args[i]) && args.length > i + 1)
          nomeArquivoPeriodos = args[i + 1];

        // Procura pela opção -d, que especifica o arquivo de docentes.
        else if ("-d".equals(args[i]) && args.length > i + 1)
          nomeArquivoDocentes = args[i + 1];

        // Procura pela opção -o, que especifica o arquivo de disciplinas (oferta).
        else if ("-o".equals(args[i]) && args.length > i + 1)
          nomeArquivoDisciplinas = args[i + 1];

        // Procura pela opção -e, que especifica o arquivo de estudantes.
        else if ("-e".equals(args[i]) && args.length > i + 1)
          nomeArquivoEstudantes = args[i + 1];

        // Procura pela opção -m, que especifica o arquivo de matrículas.
        else if ("-m".equals(args[i]) && args.length > i + 1)
          nomeArquivoMatriculas = args[i + 1];

        // Procura pela opção -a, que especifica o arquivo de atividades.
        else if ("-a".equals(args[i]) && args.length > i + 1)
          nomeArquivoAtividades = args[i + 1];

        // Procura pela opção -n, que especifica o arquivo de avaliações de atividades (notas).
        else if ("-n".equals(args[i]) && args.length > i + 1)
          nomeArquivoAvaliacoes = args[i + 1];

        // Procura pela opções --read-only e --write-only, que indicam o uso de serialização.
        else if ("--read-only".equals(args[i]))
          readOnly = true;
        else if ("--write-only".equals(args[i]))
          writeOnly = true;
      }

      // Cria o escritor.
      Escritor escritor = new Escritor(new File(ARQUIVO_SERIALIZACAO), new File(RELATORIO_GERAL),
          new File(RELATORIO_DOCENTES), new File(RELATORIO_ESTUDANTES),
          new File(RELATORIO_DISCIPLINAS));

      // Se os nomes dos arquivos não foram especificados, imprime mensagem de erro.
      if (!writeOnly && (nomeArquivoPeriodos == null || nomeArquivoDocentes == null
          || nomeArquivoDisciplinas == null || nomeArquivoEstudantes == null
          || nomeArquivoMatriculas == null || nomeArquivoAtividades == null
          || nomeArquivoAvaliacoes == null))
        System.out.printf(
            "Arquivos de entrada não especificados. Use: -p <arquivo> -d <arquivo> -o <arquivo> -e <arquivo> -m <arquivo> -a <arquivo> -n <arquivo>%n");

      // Do contrário, executa a aplicação. Verifica primeiro se devemos restaurá-la por
      // serialização.
      else if (writeOnly) {
        apl = escritor.desserializar();
        apl.serializar = false;
      }

      // Se não é pra restaurar a aplicação serializada, cria uma nova aplicação e lê os dados dos
      // arquivos.
      else {
        Leitor leitor = new Leitor(new File(nomeArquivoPeriodos), new File(nomeArquivoDocentes),
            new File(nomeArquivoDisciplinas), new File(nomeArquivoEstudantes),
            new File(nomeArquivoMatriculas), new File(nomeArquivoAtividades),
            new File(nomeArquivoAvaliacoes));
        apl = new AplAvaliaEarte(readOnly, leitor.getDocentes(), leitor.getDisciplinas(), leitor.getEstudantes());
      }

      // Finalmente, executa a aplicação.
      if (apl != null)
        apl.executar(escritor);
    } catch (IOException | ClassNotFoundException e) {
      System.out.println("Erro de I/O.");
      //e.printStackTrace();
    } catch (DomainException e) {
      System.out.printf("%s%n", e.getMessage());
      //e.printStackTrace();
    }
    
    //System.out.println("End");
  }

  /** Construtor. */
  public AplAvaliaEarte(boolean readOnly, Set<Docente> docentes, Set<Disciplina> disciplinas, Set<Estudante> estudantes) {
    this.serializar = readOnly;
    this.docentes = docentes;
    this.disciplinas = disciplinas;
    this.estudantes = estudantes;
  }

  /**
   * Produz a saí­da, seja serializando a aplicação (read-only) ou escrevendo os relatórios.
   * 
   * @param escritor Objeto responsável pela escrita dos relatórios.
   * @throws IOException No caso de erros de escrita (acesso aos arquivos de relatório).
   */
  private void executar(Escritor escritor) throws IOException {
    // Verifica se é pra serializar ou pra produzir os relatórios.
    if (serializar)
      escritor.serializar(this);
    else {
      // Escreve os relatórios.
      escritor.escreverRelatorioGeral(disciplinas);
      escritor.escreverRelatorioDocentes(docentes);
      escritor.escreverRelatorioEstudantes(estudantes);
      escritor.escreverRelatorioDisciplinas(disciplinas);
    }
  }
}
