/**
 * 
 */
package br.inf.ufes.prog3.trab2020e.io;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;
import br.inf.ufes.prog3.trab2020e.dominio.Atividade;
import br.inf.ufes.prog3.trab2020e.dominio.AtividadeAula;
import br.inf.ufes.prog3.trab2020e.dominio.AtividadeEstudo;
import br.inf.ufes.prog3.trab2020e.dominio.AtividadeProva;
import br.inf.ufes.prog3.trab2020e.dominio.AtividadeTrabalho;
import br.inf.ufes.prog3.trab2020e.dominio.Avaliacao;
import br.inf.ufes.prog3.trab2020e.dominio.Disciplina;
import br.inf.ufes.prog3.trab2020e.dominio.Docente;
import br.inf.ufes.prog3.trab2020e.dominio.Estudante;
import br.inf.ufes.prog3.trab2020e.dominio.Periodo;
import br.inf.ufes.prog3.trab2020e.exceptions.CadastroRepetidoException;
import br.inf.ufes.prog3.trab2020e.exceptions.DadoInvalidoException;
import br.inf.ufes.prog3.trab2020e.exceptions.DomainException;
import br.inf.ufes.prog3.trab2020e.exceptions.ReferenciaInvalidaException;

/**
 * Classe responsável pela leitura dos dados das planilhas.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class Leitor {
  /** Formatador de datas. */
  static final DateFormat FORMATA_DATAS = new SimpleDateFormat("dd/MM/yyyy");

  /** Formatador de datas e horas. */
  static final DateFormat FORMATA_DATAS_HORAS = new SimpleDateFormat("dd/MM/yyyy HH:mm");

  /** Formatador de números. */
  static final NumberFormat FORMATA_NUMEROS = NumberFormat.getNumberInstance();

  /** Períodos cadastrados, indexados pelo seus respectivos nomes no formato ano/semestre. */
  private Map<String, Periodo> periodos = new HashMap<>();

  /** Docentes cadastrados, indexados pelo seus respectivos logins institucionais. */
  private Map<String, Docente> docentes = new HashMap<>();

  /** Disciplinas cadastradas, indexadas por código-período. */
  private Map<String, Disciplina> disciplinas = new HashMap<>();

  /** Estudantes cadastrados, indexados pelo suas respectivas matrículas. */
  private Map<Long, Estudante> estudantes = new HashMap<>();

  /** Construtor. */
  public Leitor(File arquivoPeriodos, File arquivoDocentes, File arquivoDisciplinas,
      File arquivoEstudantes, File arquivoMatriculas, File arquivoAtividades,
      File arquivoAvaliacoes) throws IOException, DomainException {
    // Lê os arquivos de dados na ordem correta.
    lerPeriodos(arquivoPeriodos);
    lerDocentes(arquivoDocentes);
    lerDisciplinas(arquivoDisciplinas);
    lerEstudantes(arquivoEstudantes);
    lerMatriculas(arquivoMatriculas);
    lerAtividades(arquivoAtividades);
    lerAvaliacoes(arquivoAvaliacoes);
  }

  /**
   * Provê o conjunto de docentes lidos dos arquivos.
   * 
   * @return Um conjunto ordenado de docentes lidos dos arquivos.
   */
  public SortedSet<Docente> getDocentes() {
    SortedSet<Docente> set = new TreeSet<>();
    set.addAll(docentes.values());
    return set;
  }

  /**
   * Provê o conjunto de disciplinas lidas dos arquivos.
   * 
   * @return Um conjunto ordenado de disciplinas lidas dos arquivos.
   */
  public SortedSet<Disciplina> getDisciplinas() {
    SortedSet<Disciplina> set = new TreeSet<>();
    set.addAll(disciplinas.values());
    return set;
  }

  /**
   * Provê o conjunto de estudantes lidos dos arquivos.
   * 
   * @return Um conjunto ordenado de estudantes lidos dos arquivos.
   */
  public SortedSet<Estudante> getEstudantes() {
    SortedSet<Estudante> set = new TreeSet<>();
    set.addAll(estudantes.values());
    return set;
  }



  /* ================== MÉTODOS DE LEITURA. ================== */

  /**
   * Lê o cadastro de períodos.
   * 
   * @param arquivo Arquivo que contém os dados do cadastro de períodos.
   * @throws IOException No caso de erros de leitura (acesso ao arquivo).
   * @throws ParseException No caso de erros de formatação dos dados do arquivo.
   */
  private void lerPeriodos(File arquivo) throws IOException, DomainException {
    // Obtém a lista de períodos do arquivo.
    List<Periodo> lista = new ArrayList<>();
    lerArquivo(arquivo, CONVERSOR_CSV_PERIODO, lista);

    // Monta o mapa de períodos por código.
    for (Periodo objeto : lista) {
      String chave = objeto.toString();

      // Verifica cadastro repetido antes de cadastrar.
      if (periodos.containsKey(chave))
        throw new CadastroRepetidoException(chave);
      periodos.put(chave, objeto);
    }
  }

  /**
   * Lê o cadastro de docentes.
   * 
   * @param arquivo Arquivo que contém os dados do cadastro de docentes.
   * @throws IOException No caso de erros de leitura (acesso ao arquivo).
   * @throws ParseException No caso de erros de formatação dos dados do arquivo.
   */
  private void lerDocentes(File arquivo) throws IOException, DomainException {
    // Obtém a lista de docentes do arquivo.
    List<Docente> lista = new ArrayList<>();
    lerArquivo(arquivo, CONVERSOR_CSV_DOCENTE, lista);

    // Monta o mapa de docentes por login.
    for (Docente objeto : lista) {
      String chave = objeto.getLogin();

      // Verifica cadastro repetido antes de cadastrar.
      if (docentes.containsKey(chave))
        throw new CadastroRepetidoException(chave);
      docentes.put(chave, objeto);
    }
  }

  /**
   * Lê o cadastro de disciplinas.
   * 
   * @param arquivo Arquivo que contém os dados do cadastro de disciplinas.
   * @throws IOException No caso de erros de leitura (acesso ao arquivo).
   * @throws ParseException No caso de erros de formatação dos dados do arquivo.
   */
  private void lerDisciplinas(File arquivo) throws IOException, DomainException {
    // Obtém a lista de disciplinas do arquivo.
    List<Disciplina> lista = new ArrayList<>();
    lerArquivo(arquivo, CONVERSOR_CSV_DISCIPLINA, lista);

    // Monta o mapa de disciplinas por código-período.
    for (Disciplina objeto : lista) {
      String chave = objeto.getReferencia();

      // Verifica cadastro repetido antes de cadastrar.
      if (disciplinas.containsKey(chave))
        throw new CadastroRepetidoException(chave);
      disciplinas.put(chave, objeto);
    }
  }

  /**
   * Lê o cadastro de estudantes.
   * 
   * @param arquivo Arquivo que contém os dados do cadastro de estudantes.
   * @throws IOException No caso de erros de leitura (acesso ao arquivo).
   * @throws ParseException No caso de erros de formatação dos dados do arquivo.
   */
  private void lerEstudantes(File arquivo) throws IOException, DomainException {
    // Obtém a lista de estudantes do arquivo.
    List<Estudante> lista = new ArrayList<>();
    lerArquivo(arquivo, CONVERSOR_CSV_ESTUDANTE, lista);

    // Monta o mapa de docentes por login.
    for (Estudante objeto : lista) {
      Long chave = objeto.getMatricula();

      // Verifica cadastro repetido antes de cadastrar.
      if (estudantes.containsKey(chave))
        throw new CadastroRepetidoException("" + chave);
      estudantes.put(chave, objeto);
    }
  }

  /**
   * Lê o cadastro de matrículas.
   * 
   * @param arquivo Arquivo que contém os dados do cadastro de matrículas.
   * @throws IOException No caso de erros de leitura (acesso ao arquivo).
   * @throws ParseException No caso de erros de formatação dos dados do arquivo.
   */
  private void lerMatriculas(File arquivo) throws IOException, DomainException {
    // Lê as matrículas, vinculando estudantes e disciplinas. Não há necessidade de montar uma
    // lista.
    lerArquivo(arquivo, CONVERSOR_CSV_MATRICULA, null);
  }

  /**
   * Lê o cadastro de atividades.
   * 
   * @param arquivo Arquivo que contém os dados do cadastro de atividades.
   * @throws IOException No caso de erros de leitura (acesso ao arquivo).
   * @throws ParseException No caso de erros de formatação dos dados do arquivo.
   */
  private void lerAtividades(File arquivo) throws IOException, DomainException {
    // Lê as atividades, vinculando-as às suas respectivas disciplinas. Não há necessidade de montar
    // uma lista.
    lerArquivo(arquivo, CONVERSOR_CSV_ATIVIDADE, null);
  }

  /**
   * Lê o cadastro de avaliações.
   * 
   * @param arquivo Arquivo que contém os dados do cadastro de avaliações.
   * @throws IOException No caso de erros de leitura (acesso ao arquivo).
   * @throws ParseException No caso de erros de formatação dos dados do arquivo.
   */
  private void lerAvaliacoes(File arquivo) throws IOException, DomainException {
    // Lê as avaliações, vinculando-as aos seus respectivos estudantes e atividades. Não há
    // necessidade de montar uma lista.
    lerArquivo(arquivo, CONVERSOR_CSV_AVALIACAO, null);
  }

  /**
   * Método genérico para leitura de arquivos CSV. Os dados lidos do arquivo são convertidos para o
   * objeto específico por meio de um conversor e colocados em uma lista.
   * 
   * @param arquivo Arquivo que contém os dados em formato CSV.
   * @param conversor Conversor responsável por criar um objeto a partir de uma linha CSV.
   * @param lista Lista onde os objetos criados serão colocados.
   * @throws IOException No caso de erros de leitura (acesso ao arquivo).
   * @throws ParseException No caso de erros de formatação dos dados do arquivo.
   */
  private <T> void lerArquivo(File arquivo, ConversorCSV<T> conversor, List<T> lista)
      throws IOException, DomainException {
    // Cria um scanner para ler o arquivo linha por linha.
    try (Scanner scanner = new Scanner(arquivo)) {
      // Despreza a primeira linha (título) e lê as demais.
      if (scanner.hasNextLine())
        scanner.nextLine();
      while (scanner.hasNextLine()) {
        String linha = scanner.nextLine();
        if ((linha != null) && (!linha.isEmpty())) {
          // Separa os dados conditos na linha pelos ponto-e-vírgula usados como separadores.
          String[] dados = linha.split(";");

          // Remove espaços que estejam sobrando nas strings.
          for (int i = 0; i < dados.length; i++)
            if (dados[i] != null)
              dados[i] = dados[i].trim();

          // Cria um novo objeto a partir do conversor e adiciona-o à lista.
          conversor.criarObjetoDeLinhaCSV(dados, lista);
        }
      }
    }
  }

  /* ================== CONVERSORES CSV. ================== */

  /** Conversor CSV para períodos. */
  private final ConversorCSV<Periodo> CONVERSOR_CSV_PERIODO = new ConversorCSV<>() {
    @Override
    public void criarObjetoDeLinhaCSV(String[] dados, List<Periodo> lista) throws DomainException {
      try {
        // Verifica se o semestre foi especificado corretamente.
        if (dados[1] == null || ! (dados[1].length() == 1))
          throw new DadoInvalidoException(dados[1]);
        
        // Cria o período e adiciona à lista.
        Periodo periodo = new Periodo(Integer.valueOf(dados[0]), dados[1].charAt(0));
        lista.add(periodo);
      } catch (NumberFormatException e) {
        // Captura problema de conversão do ano para valor numérico.
        throw new DadoInvalidoException(dados[0]);
      }
    }
  };

  /** Conversor CSV para docentes. */
  private final ConversorCSV<Docente> CONVERSOR_CSV_DOCENTE = new ConversorCSV<>() {
    @Override
    public void criarObjetoDeLinhaCSV(String[] dados, List<Docente> lista) throws DomainException {
      // Cria o docente e adiciona à lista.
      String website = (dados.length > 2) ? dados[2] : "";
      Docente docente = new Docente(dados[0], dados[1], website);
      lista.add(docente);
    }
  };

  /** Conversor CSV para disciplinas. */
  private final ConversorCSV<Disciplina> CONVERSOR_CSV_DISCIPLINA = new ConversorCSV<>() {
    @Override
    public void criarObjetoDeLinhaCSV(String[] dados, List<Disciplina> lista)
        throws DomainException {
      // Obtém o período e o docente associados à disciplina.
      Periodo periodo = periodos.get(dados[0]);
      Docente docente = docentes.get(dados[3]);

      // Verifica referências inválidas.
      if (periodo == null)
        throw new ReferenciaInvalidaException(dados[0]);
      if (docente == null)
        throw new ReferenciaInvalidaException(dados[3]);

      // Cria a diciplina e adiciona à lista.
      Disciplina disciplina = new Disciplina(periodo, dados[1], dados[2], docente);
      lista.add(disciplina);
    }
  };

  /** Conversor CSV para estudante. */
  private final ConversorCSV<Estudante> CONVERSOR_CSV_ESTUDANTE = new ConversorCSV<>() {
    @Override
    public void criarObjetoDeLinhaCSV(String[] dados, List<Estudante> lista)
        throws DomainException {
      try {
        // Cria o estudante e adiciona à lista.
        Estudante estudante = new Estudante(Long.valueOf(dados[0]), dados[1]);
        lista.add(estudante);
      } catch (NumberFormatException e) {
        // Captura problema de conversão do ano para valor numérico.
        throw new DadoInvalidoException(dados[0]);
      }
    }
  };

  /** Conversor CSV para matrícula. */
  private final ConversorCSV<Object> CONVERSOR_CSV_MATRICULA = new ConversorCSV<>() {
    @Override
    public void criarObjetoDeLinhaCSV(String[] dados, List<Object> lista) throws DomainException {
      try {
        // Obtém a disciplina e o estudante.
        Disciplina disciplina = disciplinas.get(dados[0]);
        Estudante estudante = estudantes.get(Long.valueOf(dados[1]));

        // Verifica referências inválidas.
        if (disciplina == null)
          throw new ReferenciaInvalidaException(dados[0]);
        if (estudante == null)
          throw new ReferenciaInvalidaException(dados[1]);

        // Se tudo certo, matricula o estudante na disciplina.
        disciplina.matricular(estudante);
      } catch (NumberFormatException e) {
        // Captura problema de conversão da matrícula para valor numérico.
        throw new DadoInvalidoException(dados[1]);
      }

    }
  };

  /** Conversor CSV para atividade. */
  private final ConversorCSV<Atividade> CONVERSOR_CSV_ATIVIDADE = new ConversorCSV<>() {
    @Override
    public void criarObjetoDeLinhaCSV(String[] dados, List<Atividade> lista)
        throws DomainException {
      // Obtém a disciplina associada.
      Disciplina disciplina = disciplinas.get(dados[0]);

      // Verifica referência inválida.
      if (disciplina == null)
        throw new ReferenciaInvalidaException(dados[0]);

      // Verifica o tipo da atividade para ler os dados relevantes a cada uma. Ao indicar a
      // disciplina na construção das atividades, as mesmas já são associadas.
      char tipo = dados[2].charAt(0);
      switch (tipo) {
        case 'A':
          try {
            // Atividade tipo aula. Registra data e hora da aula.
            new AtividadeAula(disciplina, dados[1],
                FORMATA_DATAS_HORAS.parse(dados[3] + " " + dados[4]));
          } catch (ParseException e) {
            // Captura problema de conversão de data/hora para Date.
            throw new DadoInvalidoException(dados[3] + " " + dados[4]);
          }
          break;

        case 'E':
          // Atividade tipo estudo. Primeiro cria o estudo.
          AtividadeEstudo estudo = new AtividadeEstudo(disciplina, dados[1]);

          // Lê os materiais em formato MarkDown e separa nome e link.
          String[] tokens = dados[5].split("[\\(|\\)\\[|\\]\\(|\\)]");
          for (int i = 1; i < tokens.length; i += 4)
            estudo.cadastrarMaterial(tokens[i], tokens[i + 2]);
          break;

        case 'T':
          try {
            // Atividade tipo trabalho. Registra prazo, máximo de pessoas no grupo e carga horária.
            new AtividadeTrabalho(disciplina, dados[1], FORMATA_DATAS.parse(dados[3]),
                Integer.valueOf(dados[6]), Integer.valueOf(dados[7]));
          } catch (ParseException e) {
            // Captura problema de conversão de data para Date.
            throw new DadoInvalidoException(dados[3]);
          }
          break;

        case 'P':
          // Atividade tipo prova. Registra nome, data, hora e conteúdo.
          try {
            new AtividadeProva(disciplina, dados[1],
                FORMATA_DATAS_HORAS.parse(dados[3] + " " + dados[4]), dados[5]);
            break;
          } catch (ParseException e) {
            // Captura problema de conversão de data/hora para Date.
            throw new DadoInvalidoException(dados[3] + " " + dados[4]);
          }
      }
    }
  };

  /** Conversor CSV para avaliação. */
  private final ConversorCSV<Avaliacao> CONVERSOR_CSV_AVALIACAO = new ConversorCSV<>() {
    @Override
    public void criarObjetoDeLinhaCSV(String[] dados, List<Avaliacao> lista)
        throws DomainException {
      try {
        // Obtém a disciplina e o estudantes mencionados.
        Disciplina disciplina = disciplinas.get(dados[0]);
        Estudante estudante = estudantes.get(Long.valueOf(dados[1]));

        // Verifica referências inválidas.
        if (disciplina == null)
          throw new ReferenciaInvalidaException(dados[0]);
        if (estudante == null)
          throw new ReferenciaInvalidaException(dados[1]);

        // A partir da disciplina e o número sequencial informado, recupera a atividade.
        Atividade atividade = disciplina.recuperarAtividade(Integer.valueOf(dados[2]));

        // Registra a avaliação do estudante. Ela mesmo se vincula à atividade e ao estudante.
        new Avaliacao(estudante, atividade, FORMATA_NUMEROS.parse(dados[3]).doubleValue());
      } catch (ParseException e) {
        // Captura problema de conversão da nota para valor numérico.
        throw new DadoInvalidoException(dados[3]);
      } catch (IndexOutOfBoundsException e) {
        // Captura problema de referência inválida à atividade.
        throw new ReferenciaInvalidaException(dados[2]);
      }
    }
  };
}
