package br.inf.ufes.prog3.trab2020e.exceptions;

import br.inf.ufes.prog3.trab2020e.dominio.Atividade;
import br.inf.ufes.prog3.trab2020e.dominio.Estudante;

public class AvaliacaoRepetidaException extends DomainException {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 4111966114376617077L;

  private Estudante estudante;

  private Atividade atividade;

  public AvaliacaoRepetidaException(Estudante estudante, Atividade atividade) {
    this.estudante = estudante;
    this.atividade = atividade;
  }

  @Override
  public String getMessage() {
    return "Avaliação repetida: estudante " + estudante.getMatricula() + " para atividade "
        + atividade.getNumero() + " de " + atividade.getDisciplina().getReferencia() + ".";
  }
}
