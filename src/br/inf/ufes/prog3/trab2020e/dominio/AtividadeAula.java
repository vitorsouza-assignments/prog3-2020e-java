package br.inf.ufes.prog3.trab2020e.dominio;

import java.util.Date;

/**
 * Representa uma atividade de tipo aula: uma aula síncrona dada no contexto de uma disciplina.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class AtividadeAula extends Atividade {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 2693505621676167531L;

  /** Data e hora em que a aula está marcada. */
  private Date dataHora;

  /** Construtor. */
  public AtividadeAula(Disciplina disciplina, String nome, Date dataHora) {
    super(disciplina, nome, true);
    this.dataHora = dataHora;
  }
}
