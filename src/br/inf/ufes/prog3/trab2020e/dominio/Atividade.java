package br.inf.ufes.prog3.trab2020e.dominio;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Superclasse da hierarquia de atividades, engloba aulas, estudos, trabalhos e provas. Registra
 * aquilo que é comum a todas as atividades.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public abstract class Atividade implements Serializable {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = -8087281632672355972L;

  /** Disciplina à qual a atividade está associada. */
  private Disciplina disciplina;

  /** Nome da atividade. */
  private String nome;

  /** Determina se a atividade é síncrona (true) ou assíncrona (false). */
  private boolean sincrona;

  /** Avaliações que a atividade recebeu dos estudantes. */
  private Set<Avaliacao> avaliacoes = new HashSet<>();
  
  /** Número sequencial da atividade. */
  private int numero;

  /** Construtor. */
  public Atividade(Disciplina disciplina, String nome, boolean sincrona) {
    this.disciplina = disciplina;
    this.nome = nome;
    this.sincrona = sincrona;

    // Cadastra a atividade na disciplina.
    disciplina.cadastrarAtividade(this);
  }

  /** Getter para sincrona. */
  public boolean isSincrona() {
    return sincrona;
  }

  /** Getter para cargaHoraria, que é 2 por padrão e é sobrescrito em apenas algumas subclasses. */
  public int getCargaHoraria() {
    return 2;
  }

  /**
   * Determina se a atividade é avaliativa. Por padrão não é, atividades avaliativas devem
   * sobrescrever.
   */
  public boolean isAvaliativa() {
    return false;
  }

  /**
   * Obtém a data associada à avaliação, a ser sobrescrito pelas subclasses em que isso faz sentido.
   */
  public Date getData() {
    return null;
  }

  /**
   * Getter para avaliacoes. Retorna, no entanto, uma cópia. Não é público pois é para uso de outras
   * classes do pacote.
   */
  Set<Avaliacao> getAvaliacoes() {
    return Set.copyOf(avaliacoes);
  }

  /** Getter para numero. */
  public int getNumero() {
    return numero;
  }

  /** Setter para numero. */
  public void setNumero(int numero) {
    this.numero = numero;
  }

  /** Getter para disciplina. */
  public Disciplina getDisciplina() {
    return disciplina;
  }

  /**
   * Vincula uma avaliação à atividade. Não é pública, pois tal vinculação é feita pela própria
   * avaliação, quando a mesma é criada.
   * 
   * @param avaliacao Avaliação a ser vinculada.
   */
  void vincularAvaliacao(Avaliacao avaliacao) {
    avaliacoes.add(avaliacao);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(nome).append(" (");
    builder.append(sincrona ? "síncrona" : "assíncrona");
    builder.append(')');
    return builder.toString();
  }
}
