/**
 * 
 */
package br.inf.ufes.prog3.trab2020e.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.text.Collator;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import br.inf.ufes.prog3.trab2020e.AplAvaliaEarte;
import br.inf.ufes.prog3.trab2020e.dominio.Disciplina;
import br.inf.ufes.prog3.trab2020e.dominio.Docente;
import br.inf.ufes.prog3.trab2020e.dominio.Estudante;

/**
 * Classe responsável pela escrita de relatórios em arquivos.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class Escritor {
  /** Formatador de percentuais. */
  private static final NumberFormat FORMATA_PERCENTUAL = NumberFormat.getPercentInstance();
  static {
    FORMATA_PERCENTUAL.setMaximumFractionDigits(0);
  }
  
  /** Formatador de datas. */
  private static final DateFormat FORMATA_DATAS = new SimpleDateFormat("dd/MM/yyyy");

  /** Separador de colunas CSV. */
  private static final String SEPARADOR_CSV = ";";

  /** Domínio para e-mails de docentes. */
  private static final String DOMINIO_EMAIL = "@ufes.br";

  /** Cabeçalho do arquivo de relatório de visão geral. */
  private static final String CABECALHO_RELATORIO_GERAL =
      "Período;Código Disciplina;Disciplina;Docente Responsável;E-mail Docente;Qtd. Estudantes;Qtd. Atividades";

  /** Cabeçalho do arquivo de relatório de estatísticas dos docentes. */
  private static final String CABECALHO_RELATORIO_DOCENTES =
      "Docente;Qtd. Disciplinas;Qtd. Períodos;Média Atividades/Disciplina;% Síncronas;% Assíncronas;Média de Notas";

  /** Cabeçalho do arquivo de relatório de estatísticas dos estudantes. */
  private static final String CABECALHO_RELATORIO_ESTUDANTES =
      "Matrícula;Nome;Média Disciplinas/Período;Média Avaliações/Disciplina;Média Notas Avaliações";

  /** Cabeçalho do arquivo de relatório de estatísticas das disciplinas de cada docente. */
  private static final String CABECALHO_RELATORIO_DISCIPLINAS =
      "Docente;Período;Código;Nome;Qtd. Atividades;% Síncronas;% Assíncronas;CH;Datas Avaliações";

  /** Arquivo onde a aplicação deve ser (des)serializada. */
  private File arquivoSerializacao;

  /** Arquivo no qual será escrito o relatório PAD. */
  private File arquivoRelatorioGeral;

  /** Arquivo no qual será escrito o relatório RHA. */
  private File arquivoRelatorioDocentes;

  /** Arquivo no qual será escrito o relatório de alocação de disciplinas. */
  private File arquivoRelatorioEstudantes;

  /** Arquivo no qual será escrito o relatório do PPG. */
  private File arquivoRelatorioDisciplinas;

  /** Construtor. */
  public Escritor(File arquivoSerializacao, File arquivoRelatorioGeral,
      File arquivoRelatorioDocentes, File arquivoRelatorioEstudantes,
      File arquivoRelatorioDisciplinas) {
    this.arquivoSerializacao = arquivoSerializacao;
    this.arquivoRelatorioGeral = arquivoRelatorioGeral;
    this.arquivoRelatorioDocentes = arquivoRelatorioDocentes;
    this.arquivoRelatorioEstudantes = arquivoRelatorioEstudantes;
    this.arquivoRelatorioDisciplinas = arquivoRelatorioDisciplinas;
  }


  /**
   * Serializa a aplicação e todos os dados já calculados.
   * 
   * @param aplicacao Aplicação a ser serializada.
   * @throws IOException No caso de erros de entrada e saída na escrita.
   */
  public void serializar(AplAvaliaEarte aplicacao) throws IOException {
    try (ObjectOutputStream out =
        new ObjectOutputStream(new FileOutputStream(arquivoSerializacao))) {
      out.writeObject(aplicacao);
    }
  }

  /**
   * Desserializa a aplicação e todos os dados já calculados.
   * 
   * @return A aplicação restaurada.
   * @throws IOException No caso de erros de entrada e saída na leitura.
   * @throws ClassNotFoundException No caso de ler alguma classe no arquivo que não se encontra no
   *         classpath.
   */
  public AplAvaliaEarte desserializar() throws IOException, ClassNotFoundException {
    try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(arquivoSerializacao))) {
      return (AplAvaliaEarte) in.readObject();
    }
  }

  /**
   * Escreve o relatório de visão geral dos períodos acadêmicos.
   * 
   * @param disciplinas Conjunto de disciplinas de todos os períodos acadêmicos cadastradas no
   *        sistema.
   * @throws IOException No caso de qualquer problema na escrita do arquivo.
   */
  public void escreverRelatorioGeral(Set<Disciplina> disciplinas) throws IOException {
    // Cria um escritor em cima do arquivo.
    try (PrintWriter out = new PrintWriter(arquivoRelatorioGeral)) {
      // Escreve o título das colunas do CSV.
      out.printf("%s%n", CABECALHO_RELATORIO_GERAL);

      // Processa todas as disciplinas, gerando o relatório a partir delas.
      for (Disciplina disciplina : disciplinas) {
        Docente docente = disciplina.getDocente();
        out.printf("%s%s%s%s%s%s%s%s%s%s%d%s%d%n", disciplina.getPeriodo(), SEPARADOR_CSV,
            disciplina.getCodigo(), SEPARADOR_CSV, disciplina.getNome(), SEPARADOR_CSV,
            docente.getNome(), SEPARADOR_CSV, docente.getLogin() + DOMINIO_EMAIL, SEPARADOR_CSV,
            disciplina.calcularQtdEstudantesMatriculados(), SEPARADOR_CSV,
            disciplina.calcularQtdAtividadesPropostas());
      }
    }
  }

  /**
   * Escreve o relatório de estatísticas dos docentes.
   * 
   * @param docentes Conjunto de docentes cadastrados no sistema.
   * @throws IOException No caso de qualquer problema na escrita do arquivo.
   */
  public void escreverRelatorioDocentes(Set<Docente> docentes) throws IOException {
    // Cria um escritor em cima do arquivo.
    try (PrintWriter out = new PrintWriter(arquivoRelatorioDocentes)) {
      // Escreve o título das colunas do CSV.
      out.printf("%s%n", CABECALHO_RELATORIO_DOCENTES);

      // Processa todos os docentes, gerando o relatório a partir deles.
      for (Docente docente : docentes) {
        // Calcula os percentuais de atividades síncronas e assíncronas.
        double sincronas = docente.calcularPercentualAtividadesSincronas();
        double assincronas = (docente.verificarSeProposAtividade()) ? 1 - sincronas : 0;
        
        out.printf("%s%s%d%s%d%s%.1f%s%s%s%s%s%.1f%n", docente.getNome(), SEPARADOR_CSV,
            docente.calcularQtdDisciplinas(), SEPARADOR_CSV, docente.calcularQtdPeriodosAtivo(),
            SEPARADOR_CSV, docente.calcularMediaQtdAtividadesPorDisciplina(), SEPARADOR_CSV,
            FORMATA_PERCENTUAL.format(sincronas), SEPARADOR_CSV,
            FORMATA_PERCENTUAL.format(assincronas), SEPARADOR_CSV,
            docente.calcularMediaNotasAvaliacoes());
      }
    }
  }

  /**
   * Escreve o relatório de estatísticas dos estudantes.
   * 
   * @param estudantes Conjunto de estudantes cadastrados no sistema.
   * @throws IOException No caso de qualquer problema na escrita do arquivo.
   */
  public void escreverRelatorioEstudantes(Set<Estudante> estudantes) throws IOException {
    // Cria um escritor em cima do arquivo.
    try (PrintWriter out = new PrintWriter(arquivoRelatorioEstudantes)) {
      // Escreve o título das colunas do CSV.
      out.printf("%s%n", CABECALHO_RELATORIO_ESTUDANTES);

      // Processa todos os estudantes, gerando o relatório a partir deles.
      for (Estudante estudante : estudantes) {
        out.printf("%d%s%s%s%.1f%s%.1f%s%.1f%n", estudante.getMatricula(), SEPARADOR_CSV,
            estudante.getNome(), SEPARADOR_CSV, estudante.calcularMediaQtdDisciplinasPorPeriodo(),
            SEPARADOR_CSV, estudante.calcularMediaQtdAvaliacoesPorDisciplina(), SEPARADOR_CSV,
            estudante.calcularMediaNotasAvaliacoes());
      }
    }
  }

  /**
   * Escreve o relatório de estatísticas das disciplinas dos docentes.
   * 
   * @param estudantes Conjunto de disciplinas cadastradas no sistema.
   * @throws IOException No caso de qualquer problema na escrita do arquivo.
   */
  public void escreverRelatorioDisciplinas(Set<Disciplina> disciplinas) throws IOException {
    // Reordena o conjunto de disciplinas.
    Set<Disciplina> ordenado = new TreeSet<>(new ComparadorDisciplinas());
    ordenado.addAll(disciplinas);

    // Cria um escritor em cima do arquivo.
    try (PrintWriter out = new PrintWriter(arquivoRelatorioDisciplinas)) {
      // Escreve o título das colunas do CSV.
      out.printf("%s%n", CABECALHO_RELATORIO_DISCIPLINAS);

      // Processa todas as disciplinas, gerando o relatório a partir delas.
      for (Disciplina disciplina : ordenado) {
        // Calcula os percentuais de atividades síncronas e assíncronas.
        int qtdAtividades = disciplina.calcularQtdAtividadesPropostas();
        double sincronas = (qtdAtividades == 0) ? 0
            : 1.0 * disciplina.calcularQtdAtividadesSincronas() / qtdAtividades;
        double assincronas = (qtdAtividades == 0) ? 0 : 1 - sincronas;
        
        // Prepara a lista de datas das avaliacoes.
        StringBuilder builder = new StringBuilder();
        for (Date data : disciplina.listarDatasAvaliacoes())
          builder.append(FORMATA_DATAS.format(data)).append(' ');

        out.printf("%s%s%s%s%s%s%s%s%d%s%s%s%s%s%d%s%s%n", disciplina.getDocente().getLogin(),
            SEPARADOR_CSV, disciplina.getPeriodo(), SEPARADOR_CSV, disciplina.getCodigo(),
            SEPARADOR_CSV, disciplina.getNome(), SEPARADOR_CSV, qtdAtividades, SEPARADOR_CSV,
            FORMATA_PERCENTUAL.format(sincronas), SEPARADOR_CSV,
            FORMATA_PERCENTUAL.format(assincronas), SEPARADOR_CSV,
            disciplina.calcularCargaHoraria(), SEPARADOR_CSV, builder.toString().trim());
      }
    }
  }
}


/**
 * Comparador de objetos Disciplina específico para o relatório de estatísticas de disciplinas.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
class ComparadorDisciplinas implements Comparator<Disciplina> {
  @Override
  public int compare(Disciplina o1, Disciplina o2) {
    // Compara primeiro por período.
    int cmp = o1.getPeriodo().compareTo(o2.getPeriodo());
    if (cmp != 0)
      return cmp;

    // Em seguida, ordem alfabética de código.
    Collator collator = Collator.getInstance(Locale.getDefault());
    collator.setStrength(Collator.PRIMARY);
    return collator.compare(o1.getCodigo(), o2.getCodigo());
  }
}
