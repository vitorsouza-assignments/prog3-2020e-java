package br.inf.ufes.prog3.trab2020e.dominio;

import java.io.Serializable;
import br.inf.ufes.prog3.trab2020e.exceptions.AvaliacaoRepetidaException;

/**
 * Representa uma avaliação feita por um estudante a uma atividade de uma disciplina em que está matriculado.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class Avaliacao implements Serializable {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = -5685570380594836900L;

  /** Estudante que fez a avaliação. */
  private Estudante estudante;
  
  /** Atividade sendo avaliada. */
  private Atividade atividade;
  
  /** Nota dada pelo estudante à atividade. */
  private double nota;

  /**
   * @param estudante
   * @param atividade
   * @param nota
   */
  public Avaliacao(Estudante estudante, Atividade atividade, double nota) throws AvaliacaoRepetidaException {
    this.estudante = estudante;
    this.atividade = atividade;
    this.nota = nota;
    
    // Vincula a avaliação ao estudante e à atividade.
    estudante.vincularAvaliacao(this);
    atividade.vincularAvaliacao(this);
  }
  
  /** Getter para nota. */
  public double getNota() {
    return nota;
  }
  
  /** Getter para atividade. */
  public Atividade getAtividade() {
    return atividade;
  }
}
