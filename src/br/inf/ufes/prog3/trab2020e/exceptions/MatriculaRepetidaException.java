package br.inf.ufes.prog3.trab2020e.exceptions;

import br.inf.ufes.prog3.trab2020e.dominio.Disciplina;
import br.inf.ufes.prog3.trab2020e.dominio.Estudante;

public class MatriculaRepetidaException extends DomainException {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = -3557722899305898745L;

  private Disciplina disciplina;

  private Estudante estudante;

  public MatriculaRepetidaException(Disciplina disciplina, Estudante estudante) {
    this.disciplina = disciplina;
    this.estudante = estudante;
  }

  @Override
  public String getMessage() {
    return "Matrícula repetida: " + estudante.getMatricula() + " em " + disciplina.getReferencia()
        + ".";
  }
}
