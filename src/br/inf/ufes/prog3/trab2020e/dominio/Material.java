package br.inf.ufes.prog3.trab2020e.dominio;

import java.io.Serializable;

/**
 * Representa um material indicado pelo docente a ser estudado em uma atividade de tipo estudo.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class Material implements Serializable {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 1800007497331518869L;

  /** Nome do material. */
  private String nome;
  
  /** Link na Internet onde o material pode ser obtido. */
  private String link;

  /** Construtor. */
  public Material(String nome, String link) {
    this.nome = nome;
    this.link = link;
  }
}
