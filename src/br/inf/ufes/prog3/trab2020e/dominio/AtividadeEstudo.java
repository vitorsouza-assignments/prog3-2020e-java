package br.inf.ufes.prog3.trab2020e.dominio;

import java.util.HashSet;
import java.util.Set;

/**
 * Representa uma atividade de tipo estudo: atividade assíncrona em que os estudantes matriculados
 * estudam um conjunto de materiais indicados pelo docente.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class AtividadeEstudo extends Atividade {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 7329523139811799637L;

  /** Conjunto de materiais disponibilizado pelo docente para estudo. */
  private Set<Material> materiais = new HashSet<>();

  /** Construtor. */
  public AtividadeEstudo(Disciplina disciplina, String nome) {
    super(disciplina, nome, false);
  }

  /**
   * Cadastra um novo material associado à atividade de estudo.
   * 
   * @param nome Nome do material a ser estudado.
   * @param link Link para o material na Internet.
   */
  public void cadastrarMaterial(String nome, String link) {
    materiais.add(new Material(nome, link));
  }
}
