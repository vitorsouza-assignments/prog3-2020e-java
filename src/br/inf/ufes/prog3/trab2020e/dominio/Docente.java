package br.inf.ufes.prog3.trab2020e.dominio;

import java.io.Serializable;
import java.text.Collator;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Representa um docente da universidade que leciona disciplinas.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class Docente implements Comparable<Docente>, Serializable {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 6514125024871627678L;

  /** Login único do docente na universidade (parte do e-mail anterior ao '@'). */
  private String login;

  /** Nome do docente. */
  private String nome;

  /** Endereço da página Web do docente na Internet. */
  private String paginaWeb;

  /** Disciplinas que o docente é responsável. */
  private Set<Disciplina> disciplinas = new HashSet<>();

  /** Construtor. */
  public Docente(String login, String nome, String paginaWeb) {
    this.login = login;
    this.nome = nome;
    this.paginaWeb = paginaWeb;
  }

  /** Getter para login. */
  public String getLogin() {
    return login;
  }

  /** Getter para nome. */
  public String getNome() {
    return nome;
  }

  /**
   * Calcula a quantidade de disciplinas que o docente é responsável.
   * 
   * @return O número de objetos Disciplina vinculados ao docente.
   */
  public int calcularQtdDisciplinas() {
    return disciplinas.size();
  }

  /**
   * Calcula a quantidade de períodos em que o docente está ativo, ou seja, em que possui
   * disciplinas associadas.
   * 
   * @return O número de períodos diferentes em que o docente possui alguma Disciplina vinculada.
   */
  public int calcularQtdPeriodosAtivo() {
    Set<Periodo> periodos = new HashSet<>();
    for (Disciplina disciplina : disciplinas)
      periodos.add(disciplina.getPeriodo());
    return periodos.size();
  }

  /**
   * Calcula a média de quantidade de atividades propostas por disciplina.
   * 
   * @return O número total de atividades propostas pelo docente em todas as disciplinas em que é
   *         responsável, dividido pela quantidade de disciplinas que é responsável.
   */
  public double calcularMediaQtdAtividadesPorDisciplina() {
    double soma = 0;
    for (Disciplina disciplina : disciplinas)
      soma += disciplina.calcularQtdAtividadesPropostas();
    return soma / disciplinas.size();
  }

  /**
   * Verifica se o docente propôs ao menos uma atividade.
   * 
   * @return True, se o docente propôs ao menos uma atividade, false do contrário.
   */
  public boolean verificarSeProposAtividade() {
    for (Disciplina disciplina : disciplinas)
      if (disciplina.calcularQtdAtividadesPropostas() > 0)
        return true;
    return false;
  }

  /**
   * Calcula o percentual de atividades síncronas dentre todas as atividades propostas pelo docente.
   * 
   * @return O número de atividades síncronas dividido pelo número de atividades total, considerando
   *         todas as atividades de todas as disciplinas pelas quais o docente é responsável.
   */
  public double calcularPercentualAtividadesSincronas() {
    double sincronas = 0;
    int total = 0;
    for (Disciplina disciplina : disciplinas) {
      sincronas += disciplina.calcularQtdAtividadesSincronas();
      total += disciplina.calcularQtdAtividadesPropostas();
    }
    return (total == 0) ? 0 : sincronas / total;
  }

  /**
   * Calcula a média das notas que o docente recebeu em todas as avaliações.
   * 
   * @return A soma das notas dividida pela quantidade de avaliações, considerando todas as
   *         avaliações de todas as atividades de todas as disciplinas pelas quais o docente é
   *         responsável.
   */
  public double calcularMediaNotasAvaliacoes() {
    double soma = 0;
    int qtd = 0;
    for (Disciplina disciplina : disciplinas)
      for (Atividade atividade : disciplina.getAtividades()) {
        Set<Avaliacao> avaliacoes = atividade.getAvaliacoes();
        qtd += avaliacoes.size();
        for (Avaliacao avaliacao : avaliacoes)
          soma += avaliacao.getNota();
      }
    return (qtd == 0) ? 0 : soma / qtd;
  }

  /**
   * Vincula a disciplina ao docente também do lado do docente, após ela ter sido criada com o
   * docente como responsável. O método não é público e deve ser usado apenas pela classe Disciplina
   * em seu construtor.
   * 
   * @param disciplina Disciplina pela qual o docente é responsável.
   */
  void vincularDisciplina(Disciplina disciplina) {
    disciplinas.add(disciplina);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(nome).append(" (").append(login).append("@ufes.br");
    if (paginaWeb != null && !paginaWeb.isEmpty())
      builder.append(", " + paginaWeb);
    builder.append(')');
    return builder.toString();
  }

  @Override
  public int compareTo(Docente o) {
    // Ordem alfabética reversa de nome.
    Collator collator = Collator.getInstance(Locale.getDefault());
    collator.setStrength(Collator.PRIMARY);
    return collator.compare(o.nome, nome);
  }
}
