package br.inf.ufes.prog3.trab2020e.dominio;

import java.io.Serializable;
import java.text.Collator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import br.inf.ufes.prog3.trab2020e.exceptions.AvaliacaoRepetidaException;

/**
 * Representa um estudante da universidade, que se matricula em disciplinas e pode avaliar as
 * atividades das mesmas.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class Estudante implements Comparable<Estudante>, Serializable {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 4097404408248115184L;

  /** Matrícula do estudante. */
  private long matricula;

  /** Nome do estudante. */
  private String nome;

  /** Disciplinas nas quais o estudante se matriculou. */
  private Set<Disciplina> disciplinas = new HashSet<>();

  /** Avaliações que o estudante fez das atividades. */
  private Map<Atividade, Avaliacao> avaliacoes = new HashMap<>();

  /** Construtor. */
  public Estudante(long matricula, String nome) {
    this.matricula = matricula;
    this.nome = nome;
  }

  /** Getter para matricula. */
  public long getMatricula() {
    return matricula;
  }

  /** Getter para nome. */
  public String getNome() {
    return nome;
  }

  /**
   * Calcula a média de disciplinas matriculadas por período acadêmico.
   * 
   * @return A quantidade de disciplinas em que o estudante foi matriculado dividida pelo número de
   *         períodos distintos destas mesmas disciplinas. Zero, se o estudante não foi matriculado
   *         em nenhuma disciplina.
   */
  public double calcularMediaQtdDisciplinasPorPeriodo() {
    Set<Periodo> periodos = new HashSet<>();
    for (Disciplina disciplina : disciplinas)
      periodos.add(disciplina.getPeriodo());
    return (periodos.isEmpty()) ? 0 : 1.0 * disciplinas.size() / periodos.size();
  }

  /**
   * Calcula a média de avaliações realizadas por disciplina.
   * 
   * @return Quantidade de avaliações feitas pelo estudante dividida pela quantidade de disciplinas
   *         em que ele se matriculou. Zero, se o estudante não foi matriculado em nenhuma
   *         disciplina.
   */
  public double calcularMediaQtdAvaliacoesPorDisciplina() {
    return (disciplinas.isEmpty()) ? 0 : 1.0 * avaliacoes.size() / disciplinas.size();
  }

  /**
   * Calcula a média de notas, considerando todas as avaliações já feitas.
   * 
   * @return Soma de todas as notas de todas as avaliações feitas pelo estudante dividido pela
   *         quantidade de avaliações feitas. Zero, se o estudante não fez avaliações.
   */
  public double calcularMediaNotasAvaliacoes() {
    double soma = 0;
    for (Avaliacao avaliacao : avaliacoes.values())
      soma += avaliacao.getNota();
    return (avaliacoes.isEmpty()) ? 0 : soma / avaliacoes.size();
  }

  /**
   * Vincula a disciplina ao estudante também do lado do estudante, após já ter sido matriculado na
   * disciplina do outro lado da relação. O método não é público e deve ser usado apenas pela classe
   * Disciplina após um estudante ser matriculado na mesma.
   * 
   * @param disciplina Disciplina na qual o estudante foi matriculado.
   */
  void confirmarMatricula(Disciplina disciplina) {
    disciplinas.add(disciplina);
  }

  /**
   * Vincula uma avaliação ao estudante. Não é pública, pois tal vinculação é feita pela própria
   * avaliação, quando a mesma é criada.
   * 
   * @param avaliacao Avaliação a ser vinculada.
   */
  void vincularAvaliacao(Avaliacao avaliacao) throws AvaliacaoRepetidaException {
    Atividade atividade = avaliacao.getAtividade();
    if (avaliacoes.containsKey(atividade))
      throw new AvaliacaoRepetidaException(this, atividade);
    avaliacoes.put(atividade, avaliacao);
  }

  @Override
  public String toString() {
    return nome + " (" + matricula + ")";
  }

  @Override
  public int compareTo(Estudante o) {
    // Compara primeiro pela quantidade de avaliações feitas, decrescente.
    int cmp = o.avaliacoes.size() - avaliacoes.size();
    if (cmp != 0)
      return cmp;


    // Em seguida, ordem alfabética de nome.
    Collator collator = Collator.getInstance(Locale.getDefault());
    collator.setStrength(Collator.PRIMARY);
    return collator.compare(nome, o.nome);
  }
}
