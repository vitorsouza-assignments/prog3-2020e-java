package br.inf.ufes.prog3.trab2020e.dominio;

import java.io.Serializable;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import br.inf.ufes.prog3.trab2020e.exceptions.MatriculaRepetidaException;

/**
 * Representa uma disciplina ofertada em um dado período (semestre acadêmico) por um docente.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class Disciplina implements Comparable<Disciplina>, Serializable {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 8529442865526563667L;

  /** Período em que a disciplina é ofertada. */
  private Periodo periodo;

  /** Código da disciplina. */
  private String codigo;

  /** Nome da disciplina. */
  private String nome;

  /** Docente responsável pela disciplina. */
  private Docente docente;

  /** Estudantes matriculados na disciplina. */
  private Set<Estudante> estudantes = new HashSet<>();

  /** Atividades associadas à disciplina. */
  private List<Atividade> atividades = new ArrayList<>();

  /** Construtor. */
  public Disciplina(Periodo periodo, String codigo, String nome, Docente docente) {
    this.periodo = periodo;
    this.codigo = codigo;
    this.nome = nome;
    this.docente = docente;

    // Vincula a disciplina ao docente.
    docente.vincularDisciplina(this);
  }

  /** Getter para periodo. */
  public Periodo getPeriodo() {
    return periodo;
  }

  /** Getter para codigo. */
  public String getCodigo() {
    return codigo;
  }

  /** Getter para nome. */
  public String getNome() {
    return nome;
  }

  /** Getter para docente. */
  public Docente getDocente() {
    return docente;
  }

  /**
   * Getter para atividades. Retorna, no entanto, uma cópia. Não é público pois é para uso de outras
   * classes do pacote.
   */
  List<Atividade> getAtividades() {
    return List.copyOf(atividades);
  }

  /**
   * Provê a referência completa a uma disciplina, no formato código-período.
   * 
   * @return A referência completa da disciplina.
   */
  public String getReferencia() {
    return codigo + "-" + periodo;
  }

  /**
   * Calcula a quantidade de estudantes matriculados na disciplina.
   * 
   * @return O número de objetos Estudante associados à disciplina.
   */
  public int calcularQtdEstudantesMatriculados() {
    return estudantes.size();
  }

  /**
   * Calcula a quantidade de atividades propostas pelo docente para a disciplina.
   * 
   * @return O número de objetos Atividade associados à disciplina.
   */
  public int calcularQtdAtividadesPropostas() {
    return atividades.size();
  }

  /**
   * Calcula a quantidade de atividades síncronas da disciplina.
   * 
   * @return O número de atividades síncronas propostas para a disciplina.
   */
  public int calcularQtdAtividadesSincronas() {
    int qtd = 0;
    for (Atividade atividade : atividades)
      if (atividade.isSincrona())
        qtd++;
    return qtd;
  }

  /**
   * Calcula a carga horária da disciplina com base nas cargas horárias das atividades.
   * 
   * @return A soma das cargas horárias das atividades da disciplina.
   */
  public int calcularCargaHoraria() {
    int soma = 0;
    for (Atividade atividade : atividades)
      soma += atividade.getCargaHoraria();
    return soma;
  }

  public Set<Date> listarDatasAvaliacoes() {
    Set<Date> avaliacoes = new TreeSet<>();
    for (Atividade atividade : atividades)
      if (atividade.isAvaliativa())
        avaliacoes.add(atividade.getData());
    return avaliacoes;
  }

  /**
   * Matricula um estudante na disciplina, i.e., adiciona-o ao conjunto de estudantes associados à
   * mesma.
   * 
   * @param estudante Estudante a ser matriculado.
   */
  public void matricular(Estudante estudante) throws MatriculaRepetidaException {
    if (estudantes.contains(estudante))
      throw new MatriculaRepetidaException(this, estudante);
    estudantes.add(estudante);
    estudante.confirmarMatricula(this);
  }

  /**
   * Cadastra uma nova atividade na disciplina. Não é pública, pois o cadastro de atividade é feito
   * pela própria atividade, quando a mesma é criada. A atividade recebe seu número sequencial.
   * 
   * @param atividade Atividade a ser cadastrada.
   */
  void cadastrarAtividade(Atividade atividade) {
    atividades.add(atividade);
    atividade.setNumero(atividades.size());
  }

  /**
   * Recupera uma atividade da disciplina, dado seu número sequencial.
   * 
   * @param numero Número sequencial da atividade desejada.
   * @return A atividade que possui o número sequencial informado.
   */
  public Atividade recuperarAtividade(int numero) {
    return atividades.get(numero - 1);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(nome).append(" (").append(codigo).append(") / ").append(periodo);
    return builder.toString();
  }

  @Override
  public int compareTo(Disciplina o) {
    // Compara primeiro por período, ordem crescente.
    int cmp = periodo.compareTo(o.periodo);
    if (cmp != 0)
      return cmp;

    // Em seguida, ordem alfabética de nome.
    Collator collator = Collator.getInstance(Locale.getDefault());
    collator.setStrength(Collator.PRIMARY);
    return collator.compare(nome, o.nome);
  }
}
