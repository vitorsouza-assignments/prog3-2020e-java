package br.inf.ufes.prog3.trab2020e.dominio;

import java.util.Date;

/**
 * Representa uma atividade de tipo trabalho prático: atividade assíncrona em que os estudantes
 * devem realizar um trabalho especificado e entregar os resultados ao docente para serem avaliados.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class AtividadeTrabalho extends AtividadeAvaliativa {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 6267697874419642388L;

  /** Máximo de componentes de um trabalho em grupo (1, se for individual). */
  private int maxGrupo;

  /** Carga horária que espera-se que os estudantes dediquem ao trabalho. */
  private int cargaHoraria;

  /** Construtor. */
  public AtividadeTrabalho(Disciplina disciplina, String nome, Date prazo, int maxGrupo,
      int cargaHoraria) {
    super(disciplina, nome, false, prazo);
    this.maxGrupo = maxGrupo;
    this.cargaHoraria = cargaHoraria;
  }
  
  @Override
  public int getCargaHoraria() {
    return cargaHoraria;
  }
}
