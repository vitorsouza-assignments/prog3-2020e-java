package br.inf.ufes.prog3.trab2020e.dominio;

import java.io.Serializable;

/**
 * Representa um período (semestre) acadêmico.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class Periodo implements Comparable<Periodo>, Serializable {
  /** Identificador único para serialização. */
  private static final long serialVersionUID = 8738913220859063193L;

  /** Ano acadêmico (ex.: 2020, no caso do semestre 2020/1). */
  private int ano;

  /** Semestre acadêmico dentro daquele ano (ex.: 1, no caso do seemstre 2020/1). */
  private char semestre;

  /** Construtor. */
  public Periodo(int ano, char semestre) {
    this.ano = ano;
    this.semestre = semestre;
  }

  @Override
  public String toString() {
    return ano + "/" + semestre;
  }

  @Override
  public int compareTo(Periodo o) {
    // Compara primeiro por ano, ordem crescente.
    int cmp = ano - o.ano;
    if (cmp != 0)
      return cmp;
    
    // Depois por semestre, ordem alfabética.
    return semestre - o.semestre;
  }
}
